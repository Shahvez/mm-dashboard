with source_data as (
    select college_mentees.* from
    {{ source('public_everett_community_college_data_set', 'mm_college_mentees')}} college_mentees
    inner join {{ source('public_everett_community_college_data_set', 'mm_colleges')}} college
    on college.college_name =  college_mentees.connected_college_name
    where college.college_name = 'Everett Community College'
)
select * from source_data