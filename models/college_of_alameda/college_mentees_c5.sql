with source_data as (
    select college_mentees.* from
    {{ source('public_college_of_alameda_data_set', 'mm_college_mentees')}} college_mentees
    inner join {{ source('public_college_of_alameda_data_set', 'mm_colleges')}} college
    on college.college_name =  college_mentees.connected_college_name
    where college.college_name = 'College of Alameda'
)
select * from source_data