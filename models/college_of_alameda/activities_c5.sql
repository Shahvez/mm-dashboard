with source_data as (
    select activities.* from
    {{ source('public_college_of_alameda_data_set', 'mm_activities')}} activities
    inner join {{ source('college_of_alameda_data_set', 'users_c5')}} users
    on activities.user_id =  users.id    
)
select * from source_data