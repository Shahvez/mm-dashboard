with source_data as (
    select users.* from
    {{ source('public_college_of_alameda_data_set', 'mm_mentees')}} users
    where users.connected_college_name = 'College of Alameda'
)
select * from source_data