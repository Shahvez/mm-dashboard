with source_data as (
    select users.* from
    {{ source('public_headerlabs_data_set', 'mm_mentees')}} users
    where users.connected_college_name = 'headerlabs'
)
select * from source_data