with source_data as (
    select activities.* from
    {{ source('public_headerlabs_data_set', 'mm_activities')}} activities
    inner join {{ source('headerlabs_data_set', 'users_c4')}} users
    on activities.user_id =  users.id    
)
select * from source_data