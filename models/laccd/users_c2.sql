with source_data as (
    select users.id, college_mentee.connected_college_name, college_mentee.college_id, users.password, users.user_type, 
        users.uuid, users.email, users.slug, users.image, users.is_active, users.is_profile_complete, users.birthdate, 
        users.first_name, users.last_name, users.created_at, users.updated_at, users.description, users.sign_in_count, 
        users.last_sign_in_at, users.last_sign_in_ip, users.other_gender_id, users.welcome_message, users.college_location,
        users.state_college_id, users.current_sign_in_at, users.current_sign_in_ip, users.state_university_id, users.is_online,
        users.age, users.gender, users.ethnicity, users.age_range from
    {{ source('public_laccd_data_set', 'mm_users')}} users
    inner join {{ source('public_laccd_data_set', 'mm_college_mentees')}} college_mentee
	on users.id = college_mentee.mentee_id
),
friend_source_data as (
	select mentees.* from source_data as mentees
    inner join {{ source('public_laccd_data_set', 'mm_colleges')}} college
    on college.id =  mentees.college_id
    where college.college_name = 'LACCD'
),
final as (
    select * from friend_source_data
	UNION ALL
	select users.id, users.connected_college_name, users.college_id, users.password, users.user_type, users.uuid, users.email, users.slug, users.image, users.is_active, 
        users.is_profile_complete, users.birthdate, users.first_name, users.last_name, users.created_at, users.updated_at,
        users.description, users.sign_in_count, users.last_sign_in_at, users.last_sign_in_ip, users.other_gender_id, 
        users.welcome_message, users.college_location, users.state_college_id, users.current_sign_in_at, users.current_sign_in_ip, 
        users.state_university_id, users.is_online, users.age, users.gender, users.ethnicity, users.age_range from
    {{ source('public_laccd_data_set', 'mm_users')}} users
        where user_type='Mentor' and connected_college_name = 'LACCD'
)
select * from final
order by id