with source_data as (
    select activities.* from
    {{ source('public_laccd_data_set', 'mm_activities')}} activities
    inner join {{ source('laccd_data_set', 'users_c2')}} users
    on activities.user_id =  users.id    
)
select * from source_data