with source_data as (
    select users.* from
    {{ source('public_laccd_data_set', 'mm_mentees')}} users
    where users.connected_college_name = 'LACCD'
)
select * from source_data