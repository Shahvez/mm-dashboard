with source_data as (
    select activities.* from
    {{ source('public_antelope_valley_college_data_set', 'mm_activities')}} activities
    inner join {{ source('antelope_valley_college_data_set', 'users_c6')}} users
    on activities.user_id =  users.id    
)
select * from source_data