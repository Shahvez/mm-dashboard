with source_data as (
    select users.* from
    {{ source('public_antelope_valley_college_data_set', 'mm_mentees')}} users
    where users.connected_college_name = 'Antelope Valley College'
)
select * from source_data