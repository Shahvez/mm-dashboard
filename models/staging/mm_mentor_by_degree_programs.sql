with source_data as (
    select users.id, users.first_name, users.last_name, users.email, old_colleges.degree, old_colleges.department, old_colleges.university, 
	    old_colleges.created_at, old_colleges.updated_at, users.gender, users.ethnicity, users.age, users.age_range, users.connected_college_name
    from {{ source('dashboard_data_set', 'mm_mentors')}} users
    inner join  {{ source('dashboard_data_set', 'mm_old_colleges')}} old_colleges
    on old_colleges.mentor_id =  users.id
),
final as (
    select * from source_data
)
select * from final
