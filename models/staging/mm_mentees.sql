with source_data as (
    select all_user.id, col_mentee.connected_college_name, col_mentee.college_id as mentee_college_id, all_user.password, 
        all_user.user_type, all_user.uuid, all_user.email, all_user.college_name, all_user.slug,
        all_user.image, all_user.is_active, all_user.is_profile_complete, all_user.birthdate, all_user.first_name, all_user.last_name,
        all_user.created_at, all_user.updated_at, all_user.college_id, all_user.description, all_user.sign_in_count, 
        all_user.last_sign_in_at, all_user.last_sign_in_ip, all_user.other_gender_id, all_user.welcome_message, all_user.college_location,
        all_user.state_college_id, all_user.current_sign_in_at, all_user.current_sign_in_ip, all_user.state_university_id,
        all_user.is_online, all_user.age, all_user.gender, all_user.ethnicity, all_user.age_range
    from {{ source('dashboard_data_set', 'mm_users')}} all_user
    join {{ source('dashboard_data_set', 'mm_college_mentees')}} col_mentee
    on all_user.id = col_mentee.mentee_id
)
select * from source_data

-- DISTINCT ON(col_mentee.mentee_id) mentee_id, all_user.*


