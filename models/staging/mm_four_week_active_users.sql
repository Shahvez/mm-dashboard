with source_data as (
    SELECT user_id as four_week_active_user from (
        SELECT user_id, count(id) as activties 
            from {{ source('dashboard_data_set', 'mm_activities')}}
            where created_at > (NOW() - INTERVAL '4 WEEK')
            group by user_id, EXTRACT(WEEK FROM created_at)) active_user
    group by user_id having count(activties) >= 4
)
select * from source_data
