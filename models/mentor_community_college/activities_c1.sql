with source_data as (
    select activities.* from
    {{ source('public_mentor_community_college_data_set', 'mm_activities')}} activities
    inner join {{ source('mentor_community_college_data_set', 'users_c1')}} users
    on activities.user_id =  users.id    
)
select * from source_data