with source_data as (
    select users.* from
    {{ source('public_mentor_community_college_data_set', 'mm_mentees')}} users
    where users.connected_college_name = 'Mentor Community College'
)
select * from source_data