with source_data as (
    select activities.* from
    {{ source('public_a2mend_data_set', 'mm_activities')}} activities
    inner join {{ source('a2mend_data_set', 'users_c3')}} users
    on activities.user_id =  users.id    
)
select * from source_data