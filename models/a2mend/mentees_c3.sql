with source_data as (
    select users.* from
    {{ source('public_a2mend_data_set', 'mm_mentees')}} users
    where users.connected_college_name = 'A2MEND'
)
select * from source_data