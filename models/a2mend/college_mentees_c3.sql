with source_data as (
    select college_mentees.* from
    {{ source('public_a2mend_data_set', 'mm_college_mentees')}} college_mentees
    inner join {{ source('public_a2mend_data_set', 'mm_colleges')}} college
    on college.college_name =  college_mentees.connected_college_name
    where college.college_name = 'A2MEND'
)
select * from source_data